class Stock < ApplicationRecord
  include Walletable
  
  has_one :wallet, :as => :owner
  has_many :portfolios
  has_many :users, :through => :portfolios, :source => :investor, :source_type => "User"
  has_many :teams, :through => :portfolios, :source => :investor, :source_type => "Team"

  validates :name, presence: true
end
