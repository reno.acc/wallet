class TransactionsController < ApplicationController
  before_action :set_new_transaction, only: %i[ top_up peer_transfer team_share withdraw_team_share stock_invest withdraw_stock_invest ]
  before_action :set_transaction, only: %i[ do_top_up do_peer_transfer do_team_share do_withdraw_team_share do_stock_invest do_withdraw_stock_invest ]

  def top_up
  end

  def do_top_up
    if @transaction.valid?
      @wallet = Wallet.find(@transaction.wallet_id)
      if @wallet.top_up @transaction.amount
        redirect_to @wallet, notice: "Transaction success"
      else
        render :top_up
      end
    else
      render :top_up
    end
  end

  def peer_transfer
  end

  def do_peer_transfer
    if @transaction.valid?
      @wallet = Wallet.find(@transaction.wallet_id)
      @target_wallet = Wallet.find(@transaction.ref_wallet_id)
      if @wallet.peer_transfer_to @target_wallet, @transaction.amount, @transaction.notes
        redirect_to @wallet, notice: "Transaction success"
      else
        render :peer_transfer
      end
    else
      render :peer_transfer
    end
  end

  def team_share
  end

  def do_team_share
    if @transaction.valid?
      @wallet = Wallet.find(@transaction.wallet_id)
      @target_wallet = Wallet.find(@transaction.ref_wallet_id)
      if @wallet.transfer_to_team @target_wallet.owner, @transaction.amount
        redirect_to @wallet, notice: "Transaction success"
      else
        render :peer_transfer
      end
    else
      render :peer_transfer
    end
  end

  def withdraw_team_share
  end

  def do_withdraw_team_share
  end

  def stock_invest
    @investor_options = get_investor_options
  end

  def do_stock_invest
    if @transaction.valid?
      @wallet = Wallet.find(@transaction.wallet_id)
      @target_wallet = Wallet.find(@transaction.ref_wallet_id)
      if @wallet.transfer_to_stock @target_wallet.owner, @transaction.amount
        redirect_to @wallet, notice: "Transaction success"
      else
        render :peer_transfer
      end
    else
      render :peer_transfer
    end
  end

  def withdraw_stock_invest
  end

  def do_withdraw_stock_invest
  end

  private
    def set_new_transaction
      @transaction = Transaction.new
    end

    def set_transaction 
      @transaction = Transaction.new(transaction_params)
    end

    def transaction_params
      params.require(:transaction).permit(:wallet_id, :ref_wallet_id, :amount, :notes)
    end

    def get_investor_options
      
    users = []
      User.order(:name).each do |u|
        users << [ u.name, u.wallet.id ]
      end 

      teams = []
      Team.order(:name).each do |t|
        teams << [ t.name, t.wallet.id ]
      end

      { "Individual" => users, "Team" => teams }
    end
end
