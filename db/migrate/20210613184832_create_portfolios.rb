class CreatePortfolios < ActiveRecord::Migration[6.1]
  def change
    create_table :portfolios do |t|
      t.references :stock, null: false, foreign_key: true
      t.references :investor, polymorphic: true

      t.timestamps
    end
  end
end
