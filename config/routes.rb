Rails.application.routes.draw do
  get 'transactions/top_up'
  post 'transactions/do_top_up'
  get 'transactions/peer_transfer'
  post 'transactions/do_peer_transfer'
  get 'transactions/team_share'
  post 'transactions/do_team_share'
  get 'transactions/stock_invest'
  post 'transactions/do_stock_invest'
  get 'transactions/withdraw_team_share'
  post 'transactions/do_withdraw_team_share'
  get 'transactions/withdraw_stock_invest'
  post 'transactions/do_withdraw_stock_invest'
  get "wallet/show"

  resources :wallet
  resources :stocks
  resources :teams
  resources :users
  root 'users#index'
end
