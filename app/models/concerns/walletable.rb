module Walletable
    extend ActiveSupport::Concern

    included do
        after_create :create_wallet
    end

    private
        def create_wallet
            self.wallet = Wallet.new
        end
end