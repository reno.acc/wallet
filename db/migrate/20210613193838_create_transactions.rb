class CreateTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :transactions do |t|
      t.string :type
      t.references :wallet, null: false, foreign_key: true
      t.integer :ref_wallet_id, null:false, foreign_key: true
      t.decimal :amount
      t.string :notes
      t.text :detail

      t.timestamps
    end
  end
end
