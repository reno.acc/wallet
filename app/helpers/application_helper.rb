module ApplicationHelper
    include ActiveSupport::NumberHelper

    def format_amount(amount)
        ActiveSupport::NumberHelper.number_to_rounded(amount, precision: 2, delimiter: '.', separator: ',')
    end
end
