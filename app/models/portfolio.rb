class Portfolio < ApplicationRecord
  belongs_to :stock
  belongs_to :investor, :polymorphic => true
end
