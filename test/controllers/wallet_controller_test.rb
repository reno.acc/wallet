require "test_helper"

class WalletControllerTest < ActionDispatch::IntegrationTest
  test "should get topup" do
    get wallet_topup_url
    assert_response :success
  end

  test "should get show" do
    get wallet_show_url
    assert_response :success
  end
end
