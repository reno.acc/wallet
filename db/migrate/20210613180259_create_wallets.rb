class CreateWallets < ActiveRecord::Migration[6.1]
  def change
    create_table :wallets do |t|
      t.references :owner
      t.string :owner_type

      t.timestamps
    end
  end
end
