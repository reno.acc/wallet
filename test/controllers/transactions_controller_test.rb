require "test_helper"

class TransactionsControllerTest < ActionDispatch::IntegrationTest
  test "should get top_up" do
    get transactions_top_up_url
    assert_response :success
  end

  test "should get team_share" do
    get transactions_team_share_url
    assert_response :success
  end

  test "should get stock_invest" do
    get transactions_stock_invest_url
    assert_response :success
  end

  test "should get withdraw_team_share" do
    get transactions_withdraw_team_share_url
    assert_response :success
  end

  test "should get withdraw_stock_invest" do
    get transactions_withdraw_stock_invest_url
    assert_response :success
  end
end
