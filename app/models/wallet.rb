class Wallet < ApplicationRecord
    belongs_to :owner, :polymorphic => true
    has_many :transactions

    DETAIL_PREFIXES = {
        :peer_transfer => { :w => "Transfer to ", :d => "Transfer from " },
        :team_share => { :w => "Join share on ", :d => "Join share by " },
        :stock_invest => { :w => "Investment on ", :d => "Investment by " },
        :withdrawal => { :w => "Withdrawal by ", :d => "Withdrawal from " }
    }

    def get_balance
        balance = 0
        self.transactions.each do |trx|
            balance += trx.type == 'Credit' ? trx.amount : (trx.amount * -1)
        end
        balance
    end

    def deposit(amount, ref_wallet, notes = nil, detail = nil)
        if ref_wallet.present?
            Credit.create!(wallet: self, amount: amount, ref_wallet_id: ref_wallet.id, notes: notes, detail: detail)
        else 
            puts "Rollback triggered at Wallet::deposit"
            raise ActiveRecord::Rollback
        end
    end

    def withdraw(amount, ref_wallet, notes = nil, detail = nil)
        if self.enough_balance? amount
            Debit.create!(wallet: self, amount: amount, ref_wallet_id: ref_wallet.id, notes: notes, detail: detail)
        else
            puts "Rollback triggered at Wallet::withdraw"
            raise ActiveRecord::Rollback
        end
    end

    def top_up(amount)
        self.transaction do
            Credit.create!(wallet: self, ref_wallet_id: 0, amount: amount, detail: "Top Up")
        end
    end

    def peer_transfer_to(target_wallet, amount, notes = nil)
        self.transfer_to(target_wallet, amount, :peer_transfer, notes)
    end

    def transfer_to_team(team, amount)
        target_wallet = team.wallet
        self.transaction do
            Membership.find_or_create_by(user: self.owner, team: team)
            self.transfer_to(target_wallet, amount, :team_share)
        end
    end

    def transfer_to_stock(stock, amount)
        target_wallet = stock.wallet
        self.transaction do
            Portfolio.find_or_create_by(investor: self.owner, stock: stock)
            self.transfer_to(target_wallet, amount, :stock_invest)
        end
    end

    def withdrawal_to(wallet_owner, amount)
        target_wallet = wallet_owner.wallet
        self.transfer_to(target_wallet, amount, :withdrawal)
    end

    private
        def transfer_to(target_wallet, amount, trx_type, notes = nil)
            dp = DETAIL_PREFIXES[trx_type]
            self.transaction do
                self.withdraw(amount, target_wallet, notes, dp[:w] + target_wallet.owner.name)
                target_wallet.deposit(amount, self, notes, dp[:d] + self.owner.name)
            end
        end

        def enough_balance?(amount)
            self.get_balance >= amount
        end
end
