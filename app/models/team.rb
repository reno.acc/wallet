class Team < ApplicationRecord
    include Walletable
    
    has_one :wallet, :as => :owner
    has_many :memberships
    has_many :portfolios, :as => :investor
    has_many :users, :through => :memberships
    has_many :stocks, :through => :portfolios

    validates :name, presence: true

    def getStockShareBalance(stock)
        transactions = Transaction.where("wallet_id = ? AND ref_wallet_id = ?", stock.wallet.id, self.wallet.id)

        balance = 0
        transactions.each do |t|
            balance += t.type == 'Credit' ? t.amount : (t.amount * -1)
        end

        return balance
    end
end
