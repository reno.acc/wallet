class Transaction < ApplicationRecord
  belongs_to :wallet
  has_one :ref_wallet, class_name: 'Wallet', foreign_key: 'ref_wallet_id'

  validates :ref_wallet_id, presence: { message: "must exist" }
  validates :ref_wallet_id, exclusion: { in: ->(user) { [ user.wallet_id ] }, message: "can't be the same with source wallet" }, if: -> { ref_wallet_id.present? }
  validates :amount, presence: true
  validates :amount, numericality: { greater_than_or_equal_to: 100000, less_than_or_equal_to: 100000000 }, if: ->{ amount.present? }
end
